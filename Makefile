setup:
	echo 'setup data' > code/setup

test:
	cat code/setup
	cat code/data
	# NOTE: This is needed to test artifacts in the project-action repository.
	rm -rf tmp/artifacts
	mkdir -p tmp/artifacts
	echo 'some data' > tmp/artifacts/artifact

lint:
	cat code/setup
	echo 'linters passed'

# TODO: release authomatically after build
release:
	git push -f origin main:release
