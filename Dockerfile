FROM ubuntu

RUN apt-get update
RUN apt-get install -yqq make

WORKDIR /project
RUN mkdir /project/code

COPY . .
